// Unit.hpp

#pragma once

#include <iostream>
#include <string>

#include "Tree.hpp"
#include "List.hpp"

namespace Unit
{
	template <class T>
	bool equals(T a, T b)
	{
		return a == b;
	}

	template <class T>
	bool verify(T expected, T got, const std::string& message)
	{
		if (equals(expected, got))
		{
			std::cout << "Passed! (" << message << ")" << " Expected: " << expected << ". Got: " << got << "." << std::endl;
			return true;
		}
		else
		{
			std::cout << "Failed! (" << message << ")" << " Expected: " << expected << ". Got: " << got << "." << std::endl;
			return false;
		}
	}

	template <>
	bool verify(bool expected, bool got, const std::string& message)
	{
		if (equals(expected, got))
		{
			std::cout << "Passed! (" << message << ")" << std::boolalpha << " Expected: " << expected << ". Got: " << std::boolalpha << got << "." << std::endl;
			return true;
		}
		else
		{
			std::cout << "Failed! (" << message << ")" << std::boolalpha << " Expected: " << expected << ". Got: " << std::boolalpha << got << "." << std::endl;
			return false;
		}
	}
}