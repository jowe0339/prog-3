// List.hpp

#pragma once

template <typename T>
class List
{
private:
	struct Node
	{
		T value;
		Node* next;
	};

public:
	List()
	{
		m_head = nullptr;
	}

	~List()
	{ 
		clear();
	}

	void pushFront(T value)
	{
		Node* temp = m_head;
		m_head = new Node{ value, temp };
	}

	void pushBack(T value)
	{
		if (!m_head)
		{
			m_head = new Node{ value, nullptr };
		}
		else
		{
			Node* node = m_head;
			while (node->next)
				node = node->next;
			node->next = new Node{ value, nullptr };
		}
	}

	void popFront()
	{
		if (m_head)
		{
			Node* newHead = m_head->next;
			delete m_head;
			m_head = newHead;
		}
	}

	void popBack()
	{
		if (m_head)
		{
			if (!m_head->next)
			{
				delete m_head;
				m_head = nullptr;
			}
			else
			{
				Node* node = m_head;
				while (node->next->next)
					node = node->next;
				delete node->next;
				node->next = nullptr;
			}
		}
	}

	void clear()
	{
		while (m_head) popBack();
	}

	bool find(T value) const
	{ 
		for (Node* node = m_head; node != nullptr; node = node->next)
			if (node->value == value)
				return true;
		return false;
	}

	int size() const
	{
		int n = 0;
		for (Node* node = m_head; node; node = node->next)
			n++;
		return n;
	}

	void map(void(*f)(T&))
	{
		for (Node* node = m_head; node; node = node->next)
			f(node->value);
	}

private:
	Node* m_head;
};