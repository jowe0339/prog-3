// Tree.hpp

#pragma once

#include "List.hpp"

template <typename T>
class Tree
{
private:
	struct Node
	{
		T value;
		Node* left;
		Node* right;
	};

public:
	Tree() 
	{
		m_root = nullptr;
	}

	~Tree()
	{ 
		clear();
	}

	void insert(T value)
	{
		_insert(value, m_root);
	}

	void clear()
	{
		_clear(m_root);
	}

	bool find(T value)
	{
		return _find(value, m_root);
	}

	int size()
	{
		return _size(m_root);
	}

	void preOrder(List<T>& list)
	{
		_preOrder(m_root, list);
	}

	void inOrder(List<T>& list)
	{
		_inOrder(m_root, list);
	}

	void postOrder(List<T>& list)
	{
		_postOrder(m_root, list);
	}

private:
	void _insert(T value, Node*& node)
	{
		if (node == nullptr)            node = new Node{ value, nullptr, nullptr };
		else if (value == node->value)  node->value = value;
		else if (value < node->value)   _insert(value, node->left);
		else if (value > node->value)   _insert(value, node->right);
	}

	bool _find(T value, Node* node) const
	{
		if	(node == nullptr)		       return false;
		else if (value == node->value) return true;
		else if (value < node->value)  return _find(value, node->left);
		else if (value > node->value)  return _find(value, node->right);
	}

	int _size(Node* node)
	{
		if (node == nullptr) return 0;
		else return 1 + _size(node->left) + _size(node->right);
	}

	void _clear(Node*& node)
	{
		if (node != nullptr)
		{
			_clear(node->left);
			_clear(node->right);
			deleteNode(node);
		}
	}

	void _preOrder(Node* node, List<T>& list)
	{
		if (node != nullptr)
		{
			list.pushBack(node->value);
			_preOrder(node->left, list);
			_preOrder(node->right, list);
		}
	}

	void _inOrder(Node* node, List<T>& list)
	{
		if (node != nullptr)
		{
			_inOrder(node->left, list);
			list.pushBack(node->value);
			_inOrder(node->right, list);
		}
	}

	void _postOrder(Node* node, List<T>& list)
	{
		if (node != nullptr)
		{
			_postOrder(node->left, list);
			_postOrder(node->right, list);
			list.pushBack(node->value);
		}
	}

	void deleteNode(Node*& node)
	{
		if (node != nullptr)
		{
			delete node;
			node = nullptr;
		}
	}

private:
	Node* m_root;
};