// main.cpp

#include <iostream>
using namespace std;

#include "Tree.hpp"
#include "List.hpp"
#include "Unit.hpp"

typedef string Name;

void printInt(int& a)
{
	cout << a << " ";
}

int main()
{
	cout << "\n - Tree\n\n";

	Tree<Name> nameTree;

	nameTree.insert("Johannes");
	nameTree.insert("Ulla");
	nameTree.insert("Robyn");

	Unit::verify(true, nameTree.find("Johannes"), "find Johannes");
	Unit::verify(false, nameTree.find("Janne"), "find Janne");
	Unit::verify(3, nameTree.size(), "size");

	cout << "\n\n - List\n\n";

	List<int> l;

	l.pushBack(1);
	l.pushBack(2);
	l.pushBack(3);

	Unit::verify(3, l.size(), "size");
	Unit::verify(true, l.find(3), "find 3");

	l.map([](int& x) { x = x * 2; });

	Unit::verify(true, l.find(6), "find 6");

	l.popBack();

	Unit::verify(false, l.find(3), "find 3");
	Unit::verify(false, l.find(1), "find 1");

	l.popFront();

	Unit::verify(false, l.find(1), "find 1");

	l.clear();

	Unit::verify(0, l.size(), "size");

	cout << "\n\n - PreOrder List\n\n";

	Tree<int> numTree;
	numTree.insert(0);
	numTree.insert(7);
	numTree.insert(8);
	numTree.insert(3);
	numTree.insert(-1);
	numTree.insert(5);
	numTree.insert(4);
	numTree.insert(5);
	numTree.insert(9);

	List<int> numList;
	numTree.preOrder(numList);

	Unit::verify(numTree.size(), numList.size(), "size");

	system("pause");
	return 0;
}